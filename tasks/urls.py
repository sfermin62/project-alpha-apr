from django.urls import path
from tasks.views import task_create, my_tasks

urlpatterns = [
    path("create/", task_create, name="create_task"),
    path("mine/", my_tasks, name="show_my_tasks"),
]
